/**
 * © Maxim "Kailo" Telezhenko, 2018
 */

#if !defined INFO_STR_SIZE
	#define INFO_STR_SIZE 256
#endif

stock static ArrayList g_aInfo = null;

stock int AddInfo(int userid, char[] str)
{
	if (g_aInfo == null)
	{
		g_aInfo = new ArrayList(ByteCountToCells(INFO_STR_SIZE) + 1);
	}
	
	int idx = g_aInfo.Push(userid), i = 0;
	while (str[i] != EOS && i < INFO_STR_SIZE - 1)
	{
		g_aInfo.Set(idx, str[i], 4 + i, true);
		++i;
	}
	g_aInfo.Set(idx, 0, 4 + i, true);

	return idx;
}

stock int FindInfo(int userid)
{
	return g_aInfo ? g_aInfo.FindValue(userid, 0) : -1;
}

stock int GetInfo(int idx, char[] buffer, int maxlength)
{
	int i = 0, c;
	while((c = g_aInfo.Get(idx, i + 4, true)) != EOS && i < maxlength - 1)
	{
		buffer[i] = c;
		++i;
	}
	buffer[i] = EOS;
	
	return i;
}

stock void DeleteInfo(int idx)
{
	g_aInfo.Erase(idx);
	if (g_aInfo.Length == 0)
	{
		delete g_aInfo;
	}
}

stock void ClearInfo()
{
	if (g_aInfo != null)
	{
		delete g_aInfo;
	}
}