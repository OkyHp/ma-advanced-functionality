#include <sourcemod>
#include <materialadmin>
#include <adminmenu>
#include <sdktools_sound>
#include <dhooks>
#include <csgo_colors>
#include <KailoMagic>

#pragma semicolon 1
#pragma newdecls required

//======================================//===============================================//
#define WARNS_COUNT 3                   // Максимальное количество отображаемых варнов.
#define ACCESS_UNBAN_FLAG ADMFLAG_UNBAN // Флаг для отображения списка последних банов.
#define ACCESS_MUTE_FLAG ADMFLAG_CHAT   // Флаг для отобрадения списка последних говорящих.
//======================================//===============================================//

#define SZF(%0) %0, sizeof(%0)

int			g_iWarnsAOpenTime,
			g_iBansCount,
			g_iUnbanType,
			g_iVLCount,
			g_iDelWarns,
			g_iAdmExpires[MAXPLAYERS+1],
			g_iChatState[MAXPLAYERS+1];
float		g_fVoiceList[MAXPLAYERS+1];
char		g_sDBPrefix[12] = "sb",
			g_sAdmData[MAXPLAYERS+1][5][256],
			g_sWarns[MAXPLAYERS+1][WARNS_COUNT][3][256],
			g_sSoundNoAccess[128],
			g_sSoundClose[32] = "buttons/combine_button7.wav";
bool		g_bLateLoad;
Handle		g_hVoice;
Database	g_hDatabase;
TopMenu		g_hAdminMenu;
KeyValues	g_hKeyValues,
			g_hKV_Reason;

public Plugin myinfo = 
{
	name = "[MA] Advanced Functionality",
	author = "OkyHp",
	version = "1.1.5",
	url = "https://hlmod.ru, https://dev-source.ru"
};

public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	g_bLateLoad = late;
	return APLRes_Success;
}

public void OnPluginStart()
{
	int iOffset = GameConfGetOffset(LoadGameConfigFile("voiceannounce_ex.games"), "OnVoiceTransmit");
	if(iOffset == -1)
	{
		SetFailState("Failed to get offset");
	}

	g_hVoice = DHookCreate(iOffset, HookType_Entity, ReturnType_Int, ThisPointer_CBaseEntity, VoicePost);

	TopMenu hTopMenu = GetAdminTopMenu();
	if(hTopMenu != null)
	{
		OnAdminMenuReady(hTopMenu);
	}

	LoadConfig();

	RegConsoleCmd("sm_admin", Callback_NoAccess);
	RegAdminCmd("sm_af_reload", Callback_ConfReload, ADMFLAG_ROOT);

	LoadTranslations("ma_advanced_functionality.phrases");
	LoadTranslations("materialadmin.phrases");

	if(g_bLateLoad)
	{
		if(!g_hDatabase)
		{
			g_hDatabase = MAGetDatabase();
		}

		for(int i = 1; i <= MaxClients; ++i)
		{
			if (IsClientInGame(i) && !IsFakeClient(i))
			{
				OnClientPostAdminCheck(i);
			}
		}

		MAGetConfigSetting("DatabasePrefix", g_sDBPrefix);
	}
}

public void OnMapStart()
{
	PrecacheSound(g_sSoundNoAccess);
	PrecacheSound(g_sSoundClose);
	ClearInfo();

	if(g_hDatabase && g_iDelWarns)
	{
		char szQuery[128];
		FormatEx(SZF(szQuery), "DELETE FROM `%s_warns` WHERE `expires` < %i", g_sDBPrefix, GetTime());
		g_hDatabase.Query(Default_SQL_Callback, szQuery, 1);
	}
}

public void Default_SQL_Callback(Database hDatabase, DBResultSet hResult, const char[] szError, any iQueryId)
{
	if(hResult == null || szError[0])
	{
		LogError("Default_SQL_Callback #%i: %s", iQueryId, szError);
	}
}

// Get MA table prefix
public void MAOnConfigSetting()
{
	MAGetConfigSetting("DatabasePrefix", g_sDBPrefix);
}

// Get MA database handler
public void MAOnConnectDatabase(Database hDatabase)
{
	if(!g_hDatabase)
	{
		g_hDatabase = hDatabase;
	}
}

// Load config data
void LoadConfig()
{
	// Get settings
	if(g_hKeyValues)
	{
		delete g_hKeyValues;
	}

	char szPath[256];
	g_hKeyValues = new KeyValues("Config");
	BuildPath(Path_SM, SZF(szPath), "configs/materialadmin/ma_advanced_functionality.cfg");
	if(!g_hKeyValues.ImportFromFile(szPath))
	{
		LogError("No found file: '%s'. Default settings are used", szPath);
	}

	g_hKeyValues.Rewind();
	g_iWarnsAOpenTime = g_hKeyValues.GetNum("Warns_Auto_Open_Time", 10);
	g_iBansCount = g_hKeyValues.GetNum("Bans_Count", 15);
	g_iUnbanType = g_hKeyValues.GetNum("Unban_Type", 1);
	g_iVLCount = g_hKeyValues.GetNum("Voice_Player_Count", 15);
	g_hKeyValues.GetString("No_Access_Sound", SZF(g_sSoundNoAccess), "buttons/button2.wav");
	g_iDelWarns = g_hKeyValues.GetNum("Delete_Warns", 1);

	// Get MA mute reasons
	if(g_hKV_Reason)
	{
		delete g_hKV_Reason;
	}

	g_hKV_Reason = new KeyValues("materialadmin");
	BuildPath(Path_SM, SZF(szPath), "configs/materialadmin/time_reason.cfg");
	if(!FileExists(szPath) || !g_hKV_Reason.ImportFromFile(szPath))
	{
		SetFailState("No found file: '%s'.", szPath);
	}
}

// Config reload
public Action Callback_ConfReload(int iClient, int iArgs) 
{
	LoadConfig();
	CGOPrintToChat(iClient, "%t", "Config_Reload");
	return Plugin_Handled;
}

// Voice speaking event
public MRESReturn VoicePost(int iClient, Handle hReturn)
{
	g_fVoiceList[iClient] = GetGameTime();
	return MRES_Ignored;
}

// Admin status check and send query on database
public void OnClientPostAdminCheck(int iClient)
{
	if(IsFakeClient(iClient))
	{
		return;
	}

	DHookEntity(g_hVoice, true, iClient);
	g_fVoiceList[iClient] = 0.0;
	g_iChatState[iClient] = 0;

	for(int i = 0; i < sizeof(g_sAdmData[]); ++i)
	{
		g_sAdmData[iClient][i][0] = 0;
	}

	for (int u = 0; u < WARNS_COUNT; ++u)
	{
		for(int i = 0; i < sizeof(g_sWarns[][]); ++i)
		{
			g_sWarns[iClient][u][i][0] = 0;
		}
	}

	if(!g_hDatabase || !GetUserFlagBits(iClient))
	{
		return;
	}

	int		iUserId = GetClientUserId(iClient);
	char	szQuery[256],
			szAuth[32];
	GetClientAuthId(iClient, AuthId_Steam2, SZF(szAuth), true);

	// Get admin data
	FormatEx(SZF(szQuery), "SELECT `user`, `srv_group`, `immunity`, `expired`, `aid` FROM `%s_admins` \
							WHERE `authid` REGEXP '^STEAM_[0-1]:%s$' LIMIT 1", g_sDBPrefix, szAuth[8]);
	g_hDatabase.Query(GetAdmData_Callback, szQuery, iUserId);

	// Get warns
	LoadWarnsData(iUserId, szAuth);
}

void LoadWarnsData(int iUserId, const char[] szAuth)
{
	char szQuery[256];
	FormatEx(SZF(szQuery), "SELECT `expires`, `reason`, `id` FROM `%s_warns` \
							INNER JOIN `%s_admins` ON `arecipient` = `aid` \
							WHERE `authid` REGEXP '^STEAM_[0-1]:%s$' LIMIT %i", g_sDBPrefix, g_sDBPrefix, szAuth[8], WARNS_COUNT);
	g_hDatabase.Query(GetWarns_Callback, szQuery, iUserId);
}

// Getting a response from the database
public void GetAdmData_Callback(Database hDatabase, DBResultSet hResult, const char[] szError, any UserId)
{
	if(hResult == null || szError[0])
	{
		LogError("GetAdmData_Callback: %s", szError);
		return;
	}
	
	int iClient = GetClientOfUserId(UserId);
	if(!iClient || !hResult.FetchRow())
	{
		return;
	}

	for(int i = 0; i < sizeof(g_sAdmData[]); ++i)
	{
		hResult.FetchString(i, g_sAdmData[iClient][i], sizeof(g_sAdmData[][]));
	}

	if(!g_sAdmData[iClient][1][0])
	{
		FormatEx(g_sAdmData[iClient][1], sizeof(g_sAdmData[][]), "%T", "No_Group", iClient);
	}

	if(g_sAdmData[iClient][2][0] == '0')
	{
		FormatEx(g_sAdmData[iClient][2], sizeof(g_sAdmData[][]), "%i", GetAdminImmunityLevel(GetUserAdmin(iClient)));
	}

	int iExpires = StringToInt(g_sAdmData[iClient][3]);
	if(!iExpires)
	{
		FormatEx(g_sAdmData[iClient][3], sizeof(g_sAdmData[][]), "%T", "Forever", iClient);

		g_iAdmExpires[iClient] = 0;
	}
	else
	{
		char szExpired[32];
		FormatTime(SZF(szExpired), "%d.%m.%Y, %H:%M", iExpires);
		strcopy(g_sAdmData[iClient][3], sizeof(g_sAdmData[][]), szExpired);

		g_iAdmExpires[iClient] = iExpires - GetTime();
	}
}

public void GetWarns_Callback(Database hDatabase, DBResultSet hResult, const char[] szError, any UserId)
{
	if(hResult == null || szError[0])
	{
		LogError("GetWarns_Callback: %s", szError);
		return;
	}
	
	int iClient = GetClientOfUserId(UserId);
	if(!iClient)
	{
		return;
	}
	
	int u = 0;
	while(hResult.FetchRow())
	{
		for(int i = 0; i < sizeof(g_sWarns[][]); ++i)
		{
			hResult.FetchString(i, g_sWarns[iClient][u][i], sizeof(g_sWarns[][][]));
		}

		if(g_sWarns[iClient][u][0][0] == '-')
		{
			FormatEx(g_sWarns[iClient][u][0], sizeof(g_sWarns[][][]), "%T", "Menu_Expired", iClient);
		}
		else
		{
			char szDate[32];
			FormatTime(SZF(szDate), "%d.%m.%Y", StringToInt(g_sWarns[iClient][u][0]));
			strcopy(g_sWarns[iClient][u][0], sizeof(g_sWarns[][][]), szDate);
		}

		++u;
	}

	//Auto open warns menu
	if(g_iWarnsAOpenTime && g_sWarns[iClient][0][0][0])
	{
		CreateTimer(float(g_iWarnsAOpenTime), Timer_AutoOpenWarns, GetClientUserId(iClient), TIMER_FLAG_NO_MAPCHANGE);
	}
}

/*** Admin-menu ***/
public void OnAdminMenuReady(Handle aTopMenu)
{
	TopMenu hTopMenu = TopMenu.FromHandle(aTopMenu);
	if(hTopMenu == g_hAdminMenu) return;
	g_hAdminMenu = hTopMenu;

	// Create information category
	TopMenuObject hCategory = g_hAdminMenu.AddCategory("ma_informaton_category", Handler_MenuMAInformation, "ma_admim_informaton", ADMFLAG_GENERIC);
	if(hCategory != INVALID_TOPMENUOBJECT)
	{
		g_hAdminMenu.AddItem("ma_admin_warns", Handler_MenuMAWarns, hCategory, "ma_admin_warns", ADMFLAG_GENERIC);
		g_hAdminMenu.AddItem("ma_admin_info", Handler_MenuMAAdminData, hCategory, "ma_admin_info", ADMFLAG_GENERIC);
		g_hAdminMenu.AddItem("ma_warns_control", Handler_MenuMAWarnsControl, hCategory, "ma_warns_control", ADMFLAG_ROOT);
	}

	// Find MA category
	if(!CheckMACategory())
	{
		CreateTimer(5.0, Timer_CheckMACategory, _, TIMER_REPEAT);
	}
}

public Action Timer_CheckMACategory(Handle hTimer)
{
	if (CheckMACategory())
	{
		return Plugin_Stop;
	}
	return Plugin_Continue;
}

bool CheckMACategory()
{
	TopMenuObject hCategory = g_hAdminMenu.FindCategory("materialadmin");
	if(hCategory != INVALID_TOPMENUOBJECT)
	{
		g_hAdminMenu.AddItem("ma_last_bans", Handler_MenuMALastBans, hCategory, "ma_last_bans", ACCESS_UNBAN_FLAG);
		g_hAdminMenu.AddItem("ma_last_speak", Handler_MenuMALastSpeak, hCategory, "ma_last_speak", ACCESS_MUTE_FLAG);
		return true;
	}
	return false;
}

// Information category handler
public void Handler_MenuMAInformation(TopMenu hMenu, TopMenuAction action, TopMenuObject object_id, int iClient, char[] szBuffer, int maxlength)
{
	if(action == TopMenuAction_DisplayOption || action == TopMenuAction_DisplayTitle)
	{
		FormatEx(szBuffer, maxlength, "%T", "Category_info", iClient);
	}
}

// MA category handler
public void Handler_MaterialAdmin(TopMenu hMenu, TopMenuAction action, TopMenuObject object_id, int iClient, char[] szBuffer, int maxlength)
{
	if(action == TopMenuAction_DisplayOption || action == TopMenuAction_DisplayTitle)
	{
		FormatEx(szBuffer, maxlength, "%T", "AdminMenu_Main", iClient);
	}
}

// Handler adm menu with admin information
public void Handler_MenuMAAdminData(TopMenu hMenu, TopMenuAction action, TopMenuObject object_id, int iClient, char[] szBuffer, int maxlength)
{
	switch (action)
	{
		case TopMenuAction_DisplayOption: FormatEx(szBuffer, maxlength, "%T", "Adm_Info", iClient);
		case TopMenuAction_SelectOption: AdmInfoMenu(iClient);
	}
}

// Admin info menu
void AdmInfoMenu(int iClient)
{
	SetGlobalTransTarget(iClient);
	char szBuffer[512],
		 szExpired[64];

	Panel hPanel = new Panel();

	FormatEx(SZF(szBuffer), "[ %t ]\n \n", "Adm_Info");
	hPanel.SetTitle(szBuffer);

	if(g_sAdmData[iClient][0][0])
	{
		if(g_iAdmExpires[iClient])
		{
			GetStringTime(g_iAdmExpires[iClient], iClient, SZF(szExpired));
			Format(SZF(szExpired), "[ %s ]", szExpired);
		}

		FormatEx(SZF(szBuffer), "%t\n ", "Adm_Info_Body", g_sAdmData[iClient][0], g_sAdmData[iClient][1], g_sAdmData[iClient][2], g_sAdmData[iClient][3], szExpired);
	}
	else
	{
		FormatEx(SZF(szBuffer), "%t\n ", "Error_Retrieving_Data");
	}

	hPanel.DrawText(szBuffer);

	FormatEx(SZF(szBuffer), "%t", "Close");
	hPanel.CurrentKey = 9;
	hPanel.DrawItem(szBuffer);

	hPanel.Send(iClient, Handler_AdmInfo, MENU_TIME_FOREVER);
	delete hPanel;
}

// Admin info panel handler
public int Handler_AdmInfo(Menu hPanel, MenuAction action, int iClient, int iOption)
{
	if(IsClientInGame(iClient) && !IsFakeClient(iClient))
	{
		EmitSoundToClient(iClient, g_sSoundClose, SOUND_FROM_PLAYER, SNDCHAN_STATIC);
	}
}

// Handler adm menu with warns
public void Handler_MenuMAWarns(TopMenu hMenu, TopMenuAction action, TopMenuObject object_id, int iClient, char[] szBuffer, int maxlength)
{
	switch (action)
	{
		case TopMenuAction_DisplayOption: FormatEx(szBuffer, maxlength, "%T", "Warns", iClient);
		case TopMenuAction_SelectOption: WarnsMenu(iClient, MENU_TIME_FOREVER);
	}
}

// Auto open warns menu
public Action Timer_AutoOpenWarns(Handle hTimer, any UserId)
{
	int iClient = GetClientOfUserId(UserId);
	if(iClient)
	{
		WarnsMenu(iClient, 10);
	}
	return Plugin_Stop;
}

// Warns menu
void WarnsMenu(int iClient, int iTime)
{
	Menu hMenu = new Menu(Handler_WarnsMenu);
	hMenu.SetTitle("%t", "Title_Warns", "Warns");

	char szBuffer[256];
	if(g_sWarns[iClient][0][0][0])
	{
		for (int i = 0; i < WARNS_COUNT; ++i)
		{
			if(!g_sWarns[iClient][i][0][0]) break;
			FormatEx(SZF(szBuffer), "%s | %s", g_sWarns[iClient][i][1], g_sWarns[iClient][i][0]);
			hMenu.AddItem(NULL_STRING, szBuffer, ITEMDRAW_DISABLED);
		}
	}
	else
	{
		FormatEx(SZF(szBuffer), "%T", "Menu_NoWarns", iClient);
		hMenu.AddItem(NULL_STRING, szBuffer, ITEMDRAW_DISABLED);
	}

	if(iTime == MENU_TIME_FOREVER) 
	{
		hMenu.ExitBackButton = true;
	}
	hMenu.ExitButton = true;
	hMenu.Display(iClient, iTime);
}

// Wanrs menu handler
public int Handler_WarnsMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch (action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: 
		{
			if(iItem == MenuCancel_ExitBack && g_hAdminMenu)
			{
				g_hAdminMenu.Display(iClient, TopMenuPosition_LastCategory);
			}
		}
	}
}

// Bans list send query
public void Handler_MenuMALastBans(TopMenu hMenu, TopMenuAction action, TopMenuObject object_id, int iClient, char[] szBuffer, int maxlength)
{
	switch (action)
	{
		case TopMenuAction_DisplayOption: FormatEx(szBuffer, maxlength, "%T", "LastBansList", iClient);
		case TopMenuAction_SelectOption: OueryMABans(iClient);
	}
}

// Query
void OueryMABans(int iClient)
{
	if(!g_hDatabase)
	{
		CGOPrintToChat(iClient, "%t", "No_DB_Connection");
		return;
	}

	char szQuery[512];
	FormatEx(SZF(szQuery), "SELECT `i`.`authid`, `name`, `length`, `user`, `u`.`authid`, `reason` \
							FROM `%s_bans` `i` \
							INNER JOIN `%s_admins` `u` ON `i`.`aid` = `u`.`aid` \
							WHERE `RemovedOn` IS NULL \
							ORDER BY `bid` DESC LIMIT %i", g_sDBPrefix, g_sDBPrefix, g_iBansCount);
	g_hDatabase.Query(GetLastBans_Callback, szQuery, GetClientUserId(iClient));
}

// Getting a response from the database
public void GetLastBans_Callback(Database hDatabase, DBResultSet hResult, const char[] szError, any UserId)
{
	if(hResult == null || szError[0])
	{
		LogError("GetLastBans_Callback: %s", szError);
		return;
	}
	
	int iClient = GetClientOfUserId(UserId);
	if(!iClient)
	{
		return;
	}

	Menu hMenu = new Menu(Handler_BansListMenu);
	hMenu.SetTitle("[ %t ]\n ", "LastBansList");
	
	char szArr[6][256],
		 szBuffer[1024];
	while(hResult.FetchRow())
	{
		for(int i = 0; i < 6; ++i)
		{
			hResult.FetchString(i, szArr[i], sizeof(szArr[]));
		}

		if(szArr[2][0])
		{
			int iTime = StringToInt(szArr[2]);
			if(!iTime) FormatEx(SZF(szBuffer), "%T", "Forever", iClient);
			else GetStringTime(iTime, iClient, SZF(szBuffer));
			strcopy(szArr[2], sizeof(szArr[]), szBuffer);
		}

		FormatEx(SZF(szBuffer), "%s%s%s%s%s%s", szArr[0], szArr[1], szArr[2], szArr[3], szArr[4], szArr[5]);
		hMenu.AddItem(szBuffer, szArr[1]);
	}

	if(!hMenu.ItemCount)
	{
		FormatEx(SZF(szBuffer), "%T", "No_Player", iClient);
		hMenu.AddItem(NULL_STRING, szBuffer, ITEMDRAW_DISABLED);
	}

	hMenu.ExitBackButton = true;
	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

// Преобразует время в секундах в строку (by Kailo)
void GetStringTime(int iTime, int iClient, char[] szBuffer, int maxlength)
{
	static int dims[] = {60, 60, 24, 30, 365, cellmax};
	static char sign[][] = {"Sec", "Min", "H", "D", "M", "Y"};
	static char form[][] = {"%02i %T", "%02i %T", "%i %T"};
	szBuffer[0] = EOS;
	int i = 0, f = -1;
	bool cond = false;
	while (!cond)
	{
		if (f++ == 1) cond = true;
		do {
			FormatEx(szBuffer, maxlength, form[f], iTime % dims[i], sign[i], iClient);
			if (iTime /= dims[i++], iTime == 0) return;
		} while (cond);
	}
}

// Ban Info menu handler
public int Handler_BansListMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch(action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel:
		{
			if(iItem == MenuCancel_ExitBack && g_hAdminMenu)
			{
				g_hAdminMenu.Display(iClient, TopMenuPosition_LastCategory);
			}
		}
		case MenuAction_Select:
		{
			char szArr[6][256],
				 szBuffer[1024];
			GetMenuItem(hMenu, iItem, SZF(szBuffer));
			ExplodeString(szBuffer, "", SZF(szArr), sizeof(szArr[]));

			Menu hMenu2 = new Menu(Handler_UnbanMenu);
			hMenu2.SetTitle("[ %t ]\n ", "Title_BansInfo", szArr[1], szArr[5], szArr[2], szArr[3]);

			FormatEx(SZF(szBuffer), "%t", "Unban_Item");
			hMenu2.AddItem(szArr[0], szBuffer, UnbanAccess(iClient, szArr[4]) ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);

			hMenu2.ExitBackButton = true;
			hMenu2.ExitButton = true;
			hMenu2.Display(iClient, MENU_TIME_FOREVER);
		}
	}
	return 0;
}

// Check unban access
bool UnbanAccess(int iClient, char[] szBuffer)
{
	if(g_iUnbanType && !(GetUserFlagBits(iClient) & ADMFLAG_ROOT))
	{
		char szAuth[32];
		GetClientAuthId(iClient, AuthId_Steam2, SZF(szAuth), true);
		if(strcmp(szAuth[8], szBuffer[8]))
		{
			return false;
		}
	}
	return true;
}

// Unban reason menu
public int Handler_UnbanMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch(action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: 
		{
			if(iItem == MenuCancel_ExitBack) 
			{
				OueryMABans(iClient);
			}
		}
		case MenuAction_Select:
		{
			char szBuffer[32],
				 szReason[256];
			GetMenuItem(hMenu, iItem, SZF(szBuffer));

			Menu hMenu2 = new Menu(Handler_UnbanReasonMenu);
			hMenu2.SetTitle("[ %t ]\n ", "Unban_Reason");

			FormatEx(SZF(szReason), "%T", "Custom_Reason", iClient);
			hMenu2.AddItem(szBuffer, szReason);

			g_hKeyValues.Rewind();
			if(!g_hKeyValues.JumpToKey("Unban_Reason"))
			{
				LogError("Section \"Unban_Reason\" not found!");
				return 0;
			}
			g_hKeyValues.GotoFirstSubKey(false);

			do {
				g_hKeyValues.GetString(NULL_STRING, SZF(szReason));
				hMenu2.AddItem(szBuffer, szReason);
			}
			while (g_hKeyValues.GotoNextKey(false));

			if(hMenu2.ItemCount == 1)
			{
				FormatEx(SZF(szReason), " \n%T", "No_Unban_Reason", iClient);
				hMenu2.AddItem(NULL_STRING, szReason, ITEMDRAW_DISABLED);
			}

			hMenu2.ExitBackButton = true;
			hMenu2.ExitButton = true;
			hMenu2.Display(iClient, MENU_TIME_FOREVER);
		}
	}
	return 0;
}

// Unban reason menu handler
public int Handler_UnbanReasonMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch(action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel:
		{
			if(iItem == MenuCancel_ExitBack)
			{
				OueryMABans(iClient);
			}
		}
		case MenuAction_Select:
		{
			if(iItem == 0)
			{
				char szBuffer[32];
				GetMenuItem(hMenu, iItem, SZF(szBuffer));
				AddInfo(GetClientUserId(iClient), szBuffer);
				g_iChatState[iClient] = 1;
				CGOPrintToChat(iClient, "%t", "Write_In_Chat_Reason");
			}
			else
			{
				char szBuffer[32], 
					 szReason[256];
				GetMenuItem(hMenu, iItem, SZF(szBuffer), _, SZF(szReason));
				MAUnBanPlayer(iClient, szBuffer, szReason);
			}
		}
	}
	return 0;
}

// Speak list
public void Handler_MenuMALastSpeak(TopMenu hMenu, TopMenuAction action, TopMenuObject object_id, int iClient, char[] szBuffer, int maxlength)
{
	switch (action)
	{
		case TopMenuAction_DisplayOption: FormatEx(szBuffer, maxlength, "%T", "LastSpeak", iClient);
		case TopMenuAction_SelectOption: LastSpeakMenu(iClient);
	}
}

// List of recent speakers
void LastSpeakMenu(int iClient)
{
	float	fArray[MAXPLAYERS+1];
	char	szBuffer[256],
			szUserId[32];

	for(int i = 0; i < sizeof(fArray); ++i)
	{
		fArray[i] = g_fVoiceList[i];
	}
	SortFloats(SZF(fArray), Sort_Descending);

	Menu hMenu = new Menu(Handler_LastSpeakMenu);
	hMenu.SetTitle("[ %t ]\n ", "LastSpeak");

	for (int i = 0; i < sizeof(fArray); ++i)
	{
		if(!fArray[i]) break;
		for(int u = 1; u <= MaxClients; ++u)
		{
			if(hMenu.ItemCount == g_iVLCount) break;
			if(!IsClientInGame(u) || IsFakeClient(u) || fArray[i] != g_fVoiceList[u]) continue;
			
			GetClientName(u, SZF(szBuffer));
			IntToString(GetClientUserId(u), SZF(szUserId));
			hMenu.AddItem(szUserId, szBuffer);
		}
	}

	if(!hMenu.ItemCount)
	{
		FormatEx(SZF(szBuffer), "%T", "No_Player", iClient);
		hMenu.AddItem(NULL_STRING, szBuffer, ITEMDRAW_DISABLED);
	}

	hMenu.ExitBackButton = true;
	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

// Last Speak menu handler
public int Handler_LastSpeakMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch (action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: 
		{
			if(iItem == MenuCancel_ExitBack && g_hAdminMenu)
			{
				g_hAdminMenu.Display(iClient, TopMenuPosition_LastCategory);
			}
		}
		case MenuAction_Select:
		{
			char szBuffer[32],
				 szReason[256];
			GetMenuItem(hMenu, iItem, SZF(szBuffer));

			int iTarget = GetClientOfUserId(StringToInt(szBuffer));
			if(!iTarget)
			{
				CGOPrintToChat(iClient, "%t", "Player_Left_Server");
				return 0;
			}

			AdminId AdmTarget = GetUserAdmin(iTarget);
			if(AdmTarget != INVALID_ADMIN_ID && GetAdminImmunityLevel(GetUserAdmin(iClient)) < GetAdminImmunityLevel(AdmTarget))
			{
				CGOPrintToChat(iClient, "%t", "Adm_No_Access");
				return 0;
			}
			
			Menu hMenu2 = new Menu(Handler_MuteReasonMenu);
			hMenu2.SetTitle("[ %t ]\n ", "Mute_Reason");

			FormatEx(SZF(szReason), "%T", "Custom_Reason", iClient);
			hMenu2.AddItem(szBuffer, szReason);

			g_hKV_Reason.Rewind();
			if(!g_hKV_Reason.JumpToKey("MuteReasons"))
			{
				LogError("Section \"MuteReasons\" not found!");
				return 0;
			}
			g_hKV_Reason.GotoFirstSubKey(false);

			do {
				g_hKV_Reason.GetString(NULL_STRING, SZF(szReason));
				hMenu2.AddItem(szBuffer, szReason);
			}
			while (g_hKV_Reason.GotoNextKey(false));

			if(hMenu2.ItemCount == 1)
			{
				FormatEx(SZF(szReason), " \n%T", "No_Mute_Reason", iClient);
				hMenu2.AddItem(NULL_STRING, szReason, ITEMDRAW_DISABLED);
			}

			hMenu2.ExitBackButton = true;
			hMenu2.ExitButton = true;
			hMenu2.Display(iClient, MENU_TIME_FOREVER);
		}
	}
	return 0;
}

// Mute reason menu handler
public int Handler_MuteReasonMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch (action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: 
		{
			if(iItem == MenuCancel_ExitBack && g_hAdminMenu)
			{
				g_hAdminMenu.Display(iClient, TopMenuPosition_LastCategory);
			}
		}
		case MenuAction_Select:
		{
			if(iItem == 0)
			{
				char szUserId[32];
				GetMenuItem(hMenu, iItem, SZF(szUserId));
				AddInfo(GetClientUserId(iClient), szUserId);
				g_iChatState[iClient] = 2;
				CGOPrintToChat(iClient, "%t", "Write_In_Chat_Reason");
			}
			else
			{
				char szUserId[32], 
					 szReason[256];
				GetMenuItem(hMenu, iItem, SZF(szUserId), _, SZF(szReason));
				MuteTimeMenu(iClient, szUserId, szReason);
			}
		}
	}
	return 0;
}

// Time menu
void MuteTimeMenu(int iClient, const char[] szUserId, const char[] szReason)
{
	char szBuffer[512],
		 szTimeViwe[128],
		 szTime[32];

	Menu hMenu = new Menu(Handler_MuteTimeMenu);
	hMenu.SetTitle("[ %t ]\n ", "Mute_Time");

	g_hKV_Reason.Rewind();
	if(!g_hKV_Reason.JumpToKey("Time"))
	{
		LogError("Section \"Time\" not found!");
		return;
	}
	g_hKV_Reason.GotoFirstSubKey(false);

	do {
		g_hKV_Reason.GetSectionName(SZF(szTime));
		g_hKV_Reason.GetString(NULL_STRING, SZF(szTimeViwe));
		FormatEx(SZF(szBuffer), "%s%s%s", szUserId, szTime, szReason);
		hMenu.AddItem(szBuffer, szTimeViwe);
	}
	while (g_hKV_Reason.GotoNextKey(false));

	if(!hMenu.ItemCount)
	{
		FormatEx(SZF(szBuffer), " \n%T", "No_Mute_Time", iClient);
		hMenu.AddItem(NULL_STRING, szBuffer, ITEMDRAW_DISABLED);
	}

	hMenu.ExitBackButton = true;
	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

// Mute time menu handler
public int Handler_MuteTimeMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch (action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: 
		{
			if(iItem == MenuCancel_ExitBack && g_hAdminMenu)
			{
				g_hAdminMenu.Display(iClient, TopMenuPosition_LastCategory);
			}
		}
		case MenuAction_Select:
		{
			char szBuffer[512],
				 szArr[3][256];
			GetMenuItem(hMenu, iItem, SZF(szBuffer));
			ExplodeString(szBuffer, "", SZF(szArr), sizeof(szArr[]));
			
			int iTarget = GetClientOfUserId(StringToInt(szArr[0]));
			if(!iTarget)
			{
				CGOPrintToChat(iClient, "%t", "Player_Left_Server");
				return 0;
			}

			MASetClientMuteType(iClient, iTarget, szArr[2], MA_MUTE, StringToInt(szArr[1]));
		}
	}
	return 0;
}

// No access panel
public Action Callback_NoAccess(int iClient, int Args)
{
	if(iClient && IsClientInGame(iClient) && !IsFakeClient(iClient) && !GetUserFlagBits(iClient))
	{
		SetGlobalTransTarget(iClient);
		char szBuffer[512];
		Panel hPanel = new Panel();

		FormatEx(SZF(szBuffer), "%t\n \n", "Title_NoAccess");
		hPanel.SetTitle(szBuffer);
		FormatEx(SZF(szBuffer), "%t\n ", "Message_NoAccess");
		hPanel.DrawText(szBuffer);

		FormatEx(SZF(szBuffer), "%t", "Close");
		hPanel.CurrentKey = 9;
		hPanel.DrawItem(szBuffer);

		hPanel.Send(iClient, Handler_NoAccess, 30);
		delete hPanel;

		EmitSoundToClient(iClient, g_sSoundNoAccess, SOUND_FROM_PLAYER, SNDCHAN_STATIC);
	}
	return Plugin_Handled;
}

// No access panel handler
public int Handler_NoAccess(Menu hPanel, MenuAction action, int iClient, int iOption)
{
	if(IsClientInGame(iClient) && !IsFakeClient(iClient))
	{
		EmitSoundToClient(iClient, g_sSoundClose, SOUND_FROM_PLAYER, SNDCHAN_STATIC);
	}
}

//Action client say
public Action OnClientSayCommand(int iClient, const char[] command, const char[] sArgs)
{
	if(!iClient || !IsClientInGame(iClient) || !g_iChatState[iClient])
	{
		return Plugin_Continue;
	}

	if(!strcmp(sArgs, "n", false))
	{
		CGOPrintToChat(iClient, "%t", "Chat_Close");
		g_iChatState[iClient] = 0;
		return Plugin_Handled;
	}

	CGOPrintToChat(iClient, " %t", "Print_Reason", sArgs);

	int iIndex = FindInfo(GetClientUserId(iClient));
	if(iIndex != -1)
	{
		char szBuffer[INFO_STR_SIZE];
		GetInfo(iIndex, SZF(szBuffer));
		DeleteInfo(iIndex);

		switch(g_iChatState[iClient])
		{
			case 1:
			{
				char szReason[256];
				strcopy(SZF(szReason), sArgs);
				MAUnBanPlayer(iClient, szBuffer, szReason);
			}
			case 2: MuteTimeMenu(iClient, szBuffer, sArgs);
			case 3: GiveWarn(iClient, szBuffer, sArgs);
		}
	}
	else
	{
		CGOPrintToChat(iClient, " %t", "Error_Retrieving_Data");
	}

	g_iChatState[iClient] = 0;
	return Plugin_Handled;
}

// Handler warns control
public void Handler_MenuMAWarnsControl(TopMenu hMenu, TopMenuAction action, TopMenuObject object_id, int iClient, char[] szBuffer, int maxlength)
{
	switch (action)
	{
		case TopMenuAction_DisplayOption: FormatEx(szBuffer, maxlength, "%T", "Warns_Control", iClient);
		case TopMenuAction_SelectOption: WarnsControl(iClient);
	}
}

// Warns control menu
void WarnsControl(int iClient)
{
	Menu hMenu = new Menu(Handler_WarnsControl);
	hMenu.SetTitle("[ %t ]\n%t\n ", "Warns_Control", "Warns_Control_Desc");

	char szBuffer[256],
		 szUserId[32];
	// FormatEx(SZF(szBuffer), "%T\n ", "List_All_Players_Warns", iClient);
	// hMenu.AddItem(NULL_STRING, szBuffer);
	
	for(int i = 1; i <= MaxClients; ++i)
	{
		if(iClient != i && IsClientInGame(i) && !IsFakeClient(i) && GetUserFlagBits(i))
		{
			FormatEx(SZF(szBuffer), "%N %s", i, g_sWarns[i][0][0][0] ? "*" : "");
			IntToString(GetClientUserId(i), SZF(szUserId));
			hMenu.AddItem(szUserId, szBuffer);
		}
	}

	if(!hMenu.ItemCount)
	{
		FormatEx(SZF(szBuffer), "%T", "No_Admins", iClient);
		hMenu.AddItem(NULL_STRING, szBuffer, ITEMDRAW_DISABLED);
	}

	hMenu.ExitBackButton = true;
	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

// Warns control menu handler
public int Handler_WarnsControl(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch (action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: 
		{
			if(iItem == MenuCancel_ExitBack && g_hAdminMenu)
			{
				g_hAdminMenu.Display(iClient, TopMenuPosition_LastCategory);
			}
		}
		case MenuAction_Select:
		{
			// switch(iItem)
			// {
			// 	case 0:
			// 	{
			// 		CGOPrintToChat(iClient, "[MA AF] >> Use so far MA WEB for manipulations with offline players");
			// 	}
			// 	default:
			// 	{
					
			// 	}
			// }
			
			char szUserId[32];
			GetMenuItem(hMenu, iItem, SZF(szUserId));
			int iTarget = GetClientOfUserId(StringToInt(szUserId));
			if(!iTarget)
			{
				CGOPrintToChat(iClient, "%t", "Player_Left_Server");
				return 0;
			}

			Menu hMenu2 = new Menu(Handler_WarnsOnlineAdmins);
			hMenu2.SetTitle("[ %t ]\n%t", "Warns_Control", "Warns_Player_Desc", iTarget);

			char szBuffer[256];
			if(g_sWarns[iTarget][0][0][0])
			{
				for (int i = 0; i < WARNS_COUNT; ++i)
				{
					if(!g_sWarns[iTarget][i][0][0]) break;
					FormatEx(SZF(szBuffer), "%s | %s%s", g_sWarns[iTarget][i][1], g_sWarns[iTarget][i][0], !g_sWarns[iTarget][i+1][0][0] ? "\n ":"");
					hMenu2.AddItem(NULL_STRING, szBuffer, ITEMDRAW_DISABLED);
				}
			}
			else
			{
				FormatEx(SZF(szBuffer), "%T\n ", "Menu_NoWarns", iClient);
				hMenu2.AddItem(NULL_STRING, szBuffer, ITEMDRAW_DISABLED);
			}

			char szDesc[32];
			FormatEx(SZF(szDesc), "g%s", szUserId);
			FormatEx(SZF(szBuffer), "%t", "Give_Warn");
			hMenu2.AddItem(szDesc, szBuffer);
			FormatEx(SZF(szDesc), "d%s", szUserId);
			FormatEx(SZF(szBuffer), "%t", "Delete_Warn");
			hMenu2.AddItem(szDesc, szBuffer, g_sWarns[iTarget][0][0][0] ? ITEMDRAW_DEFAULT : ITEMDRAW_DISABLED);

			hMenu2.ExitBackButton = true;
			hMenu2.ExitButton = true;
			hMenu2.Display(iClient, MENU_TIME_FOREVER);
		} 
	}
	return 0;
}

// Wars control handler
public int Handler_WarnsOnlineAdmins(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch (action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: 
		{
			if(iItem == MenuCancel_ExitBack)
			{
				WarnsControl(iClient);
			}
		}
		case MenuAction_Select:
		{
			char szDesc[32];
			GetMenuItem(hMenu, iItem, SZF(szDesc));
			switch(szDesc[0])
			{
				case 'g':
				{
					AddInfo(GetClientUserId(iClient), szDesc[1]);
					g_iChatState[iClient] = 3;
					CGOPrintToChat(iClient, "%t", "Write_In_Chat_Reason_Warns");
				}
				case 'd':
				{
					int iTarget = GetClientOfUserId(StringToInt(szDesc[1]));
					if(!iTarget)
					{
						CGOPrintToChat(iClient, "%t", "Player_Left_Server");
						return 0;
					}

					Menu hMenu2 = new Menu(Handler_WarnsDeleteSelect);
					hMenu2.SetTitle("[ %t ]\n%t\n ", "Warns_Control", "Select_Warn");

					char szBuffer[256];
					for (int i = 0; i < WARNS_COUNT; ++i)
					{
						if(!g_sWarns[iTarget][i][0][0]) break;
						FormatEx(SZF(szBuffer), "%s | %s", g_sWarns[iTarget][i][1], g_sWarns[iTarget][i][0]);
						hMenu2.AddItem(szDesc[1], szBuffer);
					}

					hMenu2.ExitBackButton = true;
					hMenu2.ExitButton = true;
					hMenu2.Display(iClient, MENU_TIME_FOREVER);
				}
			}
		}
	}
	return 0;
}

// Delete warn handler
public int Handler_WarnsDeleteSelect(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch (action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: 
		{
			if(iItem == MenuCancel_ExitBack)
			{
				WarnsControl(iClient);
			}
		}
		case MenuAction_Select:
		{
			if(!g_hDatabase)
			{
				CGOPrintToChat(iClient, "%t", "No_DB_Connection");
				return 0;
			}

			char szBuffer[32],
				 szQuery[64];
			GetMenuItem(hMenu, iItem, SZF(szBuffer));
			int iUserId = StringToInt(szBuffer);
			int iTarget = GetClientOfUserId(StringToInt(szBuffer));
			if(!iTarget)
			{
				CGOPrintToChat(iClient, "%t", "Player_Left_Server");
				return 0;
			}

			FormatEx(SZF(szQuery), "DELETE FROM `%s_warns` WHERE `id` = %s", g_sDBPrefix, g_sWarns[iTarget][iItem][2]);
			g_hDatabase.Query(Default_SQL_Callback, szQuery, 2);
			CGOPrintToChat(iClient, "%t", "Sent_Query_Del_Warn");

			GetClientAuthId(iTarget, AuthId_Steam2, SZF(szBuffer), true);
			LoadWarnsData(iUserId, szBuffer);
		}
	}
	return 0;
}

// Warn time menu
void GiveWarn(int iClient, const char[] szUserId, const char[] szReason)
{
	char szBuffer[512],
		 szTimeViwe[128],
		 szTime[32];

	Menu hMenu = new Menu(Handler_WarnTimeMenu);
	hMenu.SetTitle("%t", "Give_Warn_Title_Time");

	g_hKeyValues.Rewind();
	if(!g_hKeyValues.JumpToKey("Warns_Times"))
	{
		LogError("Section \"Warns_Times\" not found!");
		return;
	}
	g_hKeyValues.GotoFirstSubKey(false);

	do {
		g_hKeyValues.GetSectionName(SZF(szTime));
		g_hKeyValues.GetString(NULL_STRING, SZF(szTimeViwe));
		FormatEx(SZF(szBuffer), "%s%s%s", szUserId, szTime, szReason);
		hMenu.AddItem(szBuffer, szTimeViwe);
	}
	while (g_hKeyValues.GotoNextKey(false));

	if(!hMenu.ItemCount)
	{
		FormatEx(SZF(szBuffer), " \n%T", "No_Wars_Time", iClient);
		hMenu.AddItem(NULL_STRING, szBuffer, ITEMDRAW_DISABLED);
	}

	hMenu.ExitBackButton = true;
	hMenu.ExitButton = true;
	hMenu.Display(iClient, MENU_TIME_FOREVER);
}

// Warn time menu handler
public int Handler_WarnTimeMenu(Menu hMenu, MenuAction action, int iClient, int iItem)
{
	switch (action)
	{
		case MenuAction_End: delete hMenu;
		case MenuAction_Cancel: 
		{
			if(iItem == MenuCancel_ExitBack)
			{
				WarnsControl(iClient);
			}
		}
		case MenuAction_Select:
		{
			if(!g_hDatabase)
			{
				CGOPrintToChat(iClient, "%t", "No_DB_Connection");
				return 0;
			}

			char szQuery[512],
				 szArr[3][256];
			GetMenuItem(hMenu, iItem, SZF(szQuery));
			ExplodeString(szQuery, "", SZF(szArr), sizeof(szArr[]));
			
			int iUserId = StringToInt(szArr[0]);
			int iTarget = GetClientOfUserId(iUserId);
			if(!iTarget || !g_sAdmData[iTarget][4][0])
			{
				CGOPrintToChat(iClient, "%t/%t", "Player_Left_Server", "Error_Retrieving_Data");
				return 0;
			}

			FormatEx(SZF(szQuery), "INSERT INTO `%s_warns` (`arecipient`, `afrom`, `expires`, `reason`) \
									VALUES (%s, 1, %i, '%s');", g_sDBPrefix, g_sAdmData[iTarget][4], (GetTime() + StringToInt(szArr[1])), szArr[2]);
			g_hDatabase.Query(GiveWarn_Callback, szQuery, iUserId);

			CGOPrintToChat(iClient, "%t", "Give_Warn_Chat_Message");
		}
	}
	return 0;
}

public void GiveWarn_Callback(Database hDatabase, DBResultSet hResult, const char[] szError, any UserId)
{
	if(hResult == null || szError[0])
	{
		LogError("GiveWarn_Callback: %s", szError);
		return;
	}
	
	int iClient = GetClientOfUserId(UserId);
	if(iClient)
	{
		ServerCommand("sm_reloadadmins");
		char szAuth[32];
		GetClientAuthId(iClient, AuthId_Steam2, SZF(szAuth), true);
		LoadWarnsData(UserId, szAuth);
	}
}