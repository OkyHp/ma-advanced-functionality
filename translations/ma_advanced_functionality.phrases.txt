"Phrases"
{
	// --------------------------------------------------------------------------------------------------
	// Тут заглавие и текст, который будет выводится игрокам без админки, при попытке ее открыть.
	// \n - Новая строка.

	"Title_NoAccess" // Заглавие
	{
		"en"		"Access is denied!"
		"ru"		"Доступ запрещен!"
		"ua"		"Доступ заборонений!"
	}

	"Message_NoAccess" // Текст
	{
		"en"		"To buy admin type in chat !donate"
		"ru"		"Для покупки админ статуса введите в чат !donate"
		"ua"		"Для покупки адмін статусу введіть в чат !donate"
	}

	// --------------------------------------------------------------------------------------------------

	"Close"
	{
		"en"		"Close"
		"ru"		"Закрыть"
		"ua"		"Закрити"
	}

	"No_Player"
	{
		"en"		"No players"
		"ru"		"Нет игроков"
		"ua"		"Немоє гравців"
	}

	"Warns"
	{
		"en"		"My warns"
		"ru"		"Мои варны"
		"ua"		"Мої варни"
	}

	"Title_Warns"
	{
		"#format"	"{1:t}"
		"en"		"[ {1} ]\n \nWarn - reprimand for breaking the rules.\nThe reason for warning / Violation | term of validity\n "
		"ru"		"[ {1} ]\n \nВарн - выговор за нарушение правил.\nПричина варна / Нарушение | Когда истекает\n "
		"ua"		"[ {1} ]\n \nВарн - виговір за нарушення правил.\nПричина варну / Порушення | Коли закінчиться\n "
	}

	"Menu_NoWarns"
	{
		"en"		"No warns"
		"ru"		"Нет варнов"
		"ua"		"Немає варнів"
	}

	"Menu_Expired"
	{
		"en"		"Expired"
		"ru"		"Истёк"
		"ua"		"Закінчився"
	}

	"Category_info"
	{
		"en"		"Information"
		"ru"		"Информация"
		"ua"		"Інформація"
	}

	"LastBansList"
	{
		"en"		"List of recent bans"
		"ru"		"Список последних банов"
		"ua"		"Список останніх банів"
	}

	"Forever"
	{
		"en"		"Forever"
		"ru"		"Навсегда"
		"ua"		"Назавжди"
	}

	"Sec"
	{
		"en"		"sec"
		"ru"		"сек"
		"ua"		"сек"
	}

	"Min"
	{
		"en"		"min"
		"ru"		"мин"
		"ua"		"хв"
	}

	"H"
	{
		"en"		"H"
		"ru"		"Ч"
		"ua"		"Ч"
	}

	"D"
	{
		"en"		"D"
		"ru"		"Д"
		"ua"		"Д"
	}

	"M"
	{
		"en"		"M"
		"ru"		"М"
		"ua"		"М"
	}

	"Y"
	{
		"en"		"Y"
		"ru"		"Г"
		"ua"		"Р"
	}

	"Title_BansInfo"
	{
		"#format"	"{1:s},{2:s},{3:s},{4:s}"
		"en"		"[ Ban Info ]\n \nPlayer: {1}\nReason: {2}\nLength: {3}\nIssued: {4}\n "
		"ru"		"[ Информация о бане ]\n \nИгрок: {1}\nПричина: {2}\nСрок: {3}\nВыдан: {4}\n "
		"ua"		"[ Інформація бану ]\n \nГравець: {1}\nПричина: {2}\nСрок: {3}\nВиданий: {4}\n "
	}

	"Unban_Item"
	{
		"en"		"Unban player"
		"ru"		"Разбанить игрока"
		"ua"		"Розбанити гравця"
	}

	"Unban_Reason"
	{
		"en"		"Unban reason"
		"ru"		"Причина для разбана"
		"ua"		"Причина для розбану"
	}

	"No_Unban_Reason"
	{
		"en"		"No reason for unban"
		"ru"		"Нет причин для разбана"
		"ua"		"Немає причин для розбану"
	}

	"Config_Reload"
	{
		"en"		"{GREEN}[MA AF] {DEFAULT}Rebooting the config"
		"ru"		"{GREEN}[MA AF] {DEFAULT}Перезагрузка конфига"
		"ua"		"{GREEN}[MA AF] {DEFAULT}Перезагрузка конфіга"
	}

	"LastSpeak"
	{
		"en"		"Latest talking players"
		"ru"		"Последние говорящие игроки"
		"ua"		"Останні гравці, що говорили"
	}

	"Mute_Reason"
	{
		"en"		"Mute reason"
		"ru"		"Причина мута"
		"ua"		"Причина муту"
	}

	"No_Mute_Reason"
	{
		"en"		"No reason for mute"
		"ru"		"Нет причин для мута"
		"ua"		"Немає причин для муту"
	}

	"Mute_Time"
	{
		"en"		"Mute time"
		"ru"		"Время мута"
		"ua"		"Час муту"
	}

	"No_Mute_Time"
	{
		"en"		"There is no time to mute"
		"ru"		"Нет времени для отключения звука"
		"ua"		"Немає часу для відключення звуку"
	}

	"Player_Left_Server"
	{
		"en"		"{GREEN}[MA AF] {DEFAULT}Player left server"
		"ru"		"{GREEN}[MA AF] {DEFAULT}Игрок покинул сервер"
		"ua"		"{GREEN}[MA AF] {DEFAULT}Гравець покинув сервер"
	}

	"No_Group"
	{
		"en"		"No Group"
		"ru"		"Нет группы"
		"ua"		"Немає групи"
	}

	"Adm_Info"
	{
		"en"		"Privilege information"
		"ru"		"Информация о привилегии"
		"ua"		"Інформація про привілеї"
	}

	"Adm_Info_Body"
	{
		"#format"	"{1:s},{2:s},{3:s},{4:s},{5:s}"
		"en"		"Login: {1}\nPrivilege: {2}\nImmunity: {3}\nExpires: {4} {5}"
		"ru"		"Логин: {1}\nПривилегия: {2}\nИммунитет: {3}\nИстекает: {4} {5}"
		"ua"		"Логін: {1}\nПривілегія: {2}\nІмунітет: {3}\nДата закінчення: {4} {5}"
	}

	"Adm_No_Access"
	{
		"en"		"{GREEN}[MA AF] {DEFAULT}No access to perform function"
		"ru"		"{GREEN}[MA AF] {DEFAULT}Нет доступа для выполнения функции"
		"ua"		"{GREEN}[MA AF] {DEFAULT}Немає доступу для виконання функції"
	}

	"Error_Retrieving_Data"
	{
		"en"		"Error retrieving data"
		"ru"		"Ошибка получения данных"
		"ua"		"Помилка отримання даних"
	}

	"Custom_Reason"
	{
		"en"		"Other reason (Custom)"
		"ru"		"Другая причина (Кастомная)"
		"ua"		"Інша причина (Кастомна)"
	}

	"Write_In_Chat_Reason"
	{
		"en"		"{GREEN}[MA AF] {DEFAULT}Type in chat the reason for suspicion. Enter {RED}N {DEFAULT}in the chat for cancel"
		"ru"		"{GREEN}[MA AF] {DEFAULT}Введите в чат причину подозрения. Для отмены введите в чат {RED}N"
		"ua"		"{GREEN}[MA AF] {DEFAULT}Введіть в чат причину підозри. Для відміни введіть в чат {RED}N"
	}

	"Chat_Close"
	{
		"en"		"{GREEN}[MA AF] {DEFAULT}Entering custom reason is canceled."
		"ru"		"{GREEN}[MA AF] {DEFAULT}Ввод кастомной причины отменён."
		"ua"		"{GREEN}[MA AF] {DEFAULT}Введення кастомної причини відмінений."
	}

	"Print_Reason"
	{
		"#format"	"{1:s}"
		"en"		"{GREEN}[MA AF] {DEFAULT}Reason: {1}"
		"ru"		"{GREEN}[MA AF] {DEFAULT}Причина: {1}"
		"ua"		"{GREEN}[MA AF] {DEFAULT}Причина: {1}"
	}

	"Warns_Control"
	{
		"en"		"Warns control"
		"ru"		"Управление варнами"
		"ua"		"Керування варнами"
	}

	"Warns_Control_Desc"
	{
		"en"		"* - Admin already have warn \nSelect admin:"
		"ru"		"* - Админ уже имеет варн \nВыберите админа:"
		"ua"		"* - Адмін вже має варн \nВиберіть адміна:"
	}

	"List_All_Players_Warns"
	{
		"en"		"List all players with a warning"
		"ru"		"Список всех игроков с варнами"
		"ua"		"Список всіх гравців з варнами"
	}

	"No_Admins"
	{
		"en"		"No admins"
		"ru"		"Нет админов"
		"ua"		"Немає адмінів"
	}

	"Players_With_Warns"
	{
		"en"		"Players with warns"
		"ru"		"Игроки с варнами"
		"ua"		"Гравці з варнами"
	}

	"Warns_Player_Desc"
	{
		"#format"	"{1:N}"
		"en"		"Admin: {1}\nWarns:\n "
		"ru"		"Админ: {1}\nВарны:\n "
		"ua"		"Адмін: {1}\nВарни:\n "
	}

	"Give_Warn"
	{
		"en"		"Give warn"
		"ru"		"Выдать варн"
		"ua"		"Видати варн"
	}

	"Delete_Warn"
	{
		"en"		"Delete warn"
		"ru"		"Удалить варн"
		"ua"		"Видалити варн"
	}

	"Write_In_Chat_Reason_Warns"
	{
		"en"		"{GREEN}[MA AF] {DEFAULT}Type in chat the violation of admin. Enter {RED}N {DEFAULT}in the chat for cancel"
		"ru"		"{GREEN}[MA AF] {DEFAULT}Введите в чат нарушение админа. Для отмены введите в чат {RED}N"
		"ua"		"{GREEN}[MA AF] {DEFAULT}Введіть в чат порушення адміна. Для відміни введіть в чат {RED}N"
	}

	"Give_Warn_Title_Time"
	{
		"en"		"[ Issuing a warning ]\n \nSelect warning period:\n "
		"ru"		"[ Выдача варна ]\n \nВыберите период варна:\n "
		"ua"		"[ Видача варну ]\n \nВиберіть період варну:\n "
	}

	"No_Wars_Time"
	{
		"en"		"No period data available for warns"
		"ru"		"Нет указанных данных о периоде для варна"
		"ua"		"Немає вказаних даних про періоди для варнів"
	}

	"Give_Warn_Chat_Message"
	{
		"en"		"{GREEN}[MA AF] {DEFAULT}Request for a warning has been sent successfully"
		"ru"		"{GREEN}[MA AF] {DEFAULT}Запрос на выдачу варна успешно отправлен"
		"ua"		"{GREEN}[MA AF] {DEFAULT}Запит на видання варну успішно відправлений"
	}

	"No_DB_Connection"
	{
		"en"		"{GREEN}[MA AF] {DEFAULT}Error! No database connection"
		"ru"		"{GREEN}[MA AF] {DEFAULT}Ошибка! Нет подключения к базе данных"
		"ua"		"{GREEN}[MA AF] {DEFAULT}Помилка! Немає з'єднання з базою даних"
	}

	"Select_Warn"
	{
		"en"		"Select the warning you want to cancel:"
		"ru"		"Выберите варн, который нужно аннулировать:"
		"ua"		"Виьеріть варн, який треба анулювати:"
	}

	"Sent_Query_Del_Warn"
	{
		"en"		"{GREEN}[MA AF] {DEFAULT}Request warning deletion successfully sent"
		"ru"		"{GREEN}[MA AF] {DEFAULT}Запрос на удаление варна успешно отправлен"
		"ua"		"{GREEN}[MA AF] {DEFAULT}Запиті на видалення варна успішно відправленний"
	}
}